import 'package:flutter/foundation.dart';
import 'package:provider/provider.dart';

class UserModel {
  //Observable
  var user$ = ValueNotifier<UserVO>(UserVO(
      name: 'Max',
      surname: 'Smith',
      email: 'msmith@gmail.com',
      phone: '+38598765432',
      profilePicture: 'https://cms.qz.com/wp-content/uploads/2018/11/lion-king-disney-e1543243298567.jpg?quality=75&strip=all&w=410&h=231'),
  );

  static List<SingleChildCloneableWidget> get provide {
    var model = UserModel();

    return [
      //singleton
      Provider<UserModel>.value(value: model),
      //observable
      ValueListenableProvider<UserVO>.value(value: model.user$)
    ];
  }
}

class UserVO {
  final String profilePicture;
  final String name;
  final String surname;
  final String email;
  final String phone;

  UserVO(
      {this.profilePicture,
      @required this.name,
      @required this.surname,
      this.email,
      this.phone});

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
          other is UserVO &&
              runtimeType == other.runtimeType &&
              profilePicture == other.profilePicture &&
              name == other.name &&
              surname == other.surname &&
              email == other.email &&
              phone == other.phone;

  @override
  int get hashCode =>
      profilePicture.hashCode ^
      name.hashCode ^
      surname.hashCode ^
      email.hashCode ^
      phone.hashCode;




}

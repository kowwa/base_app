import 'package:base_app/ui/page/home_page.dart';
import 'package:base_app/ui/page/profile_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/navigator.dart';

class Router {
  static Route onGenerateRoute(RouteSettings settings) {
    var routeName = settings.name;
    switch (routeName) {
      case Routes.home:
        return MaterialPageRoute(builder: (context) => MyHomePage());
      case Routes.profile:
        return MaterialPageRoute(builder: (context) => ProfilePage());
      default:
        return MaterialPageRoute(
            builder: (context) => Center(
                  child: Text('route missing'),
                ));
    }
  }
}

class Routes {
  static const String home = '/';

  static const String profile = "/profile";
}

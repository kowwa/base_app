import 'package:base_app/model/user_model.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:avatar_glow/avatar_glow.dart';

class UserAvatar extends StatelessWidget {
  final double radius;

  UserAvatar(this.radius);

  @override
  Widget build(BuildContext context) {
    return Hero(
      tag: 'userAvatar',
      child: Consumer<UserVO>(
        builder: (_, vo, __) => AvatarGlow(
          endRadius: radius,
          startDelay: Duration(milliseconds: 1000),
          glowColor: Colors.white,
          duration: Duration(milliseconds: 2000),
          repeat: true,
          showTwoGlows: true,
          repeatPauseDuration: Duration(milliseconds: 100),
          child: Material(
            elevation: 8.0,
            shape: CircleBorder(),
            color: Colors.transparent,
            child: CircleAvatar(
              backgroundImage:vo.profilePicture != null
                  ? Image.network(vo.profilePicture).image
                  : null,
              child: vo.profilePicture == null
                  ? Text(
                vo.name[0].toUpperCase() + vo.surname[0].toUpperCase(),
                style: Theme.of(context).textTheme.headline,
              )
                  : null,
              radius: 50.0,
            ),
          ),
        ),
      ),
    ); // profile picture or initials
  }
}

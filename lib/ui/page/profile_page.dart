import 'package:base_app/model/user_model.dart';
import 'package:base_app/ui/atom/user_avatar.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';


class ProfilePage extends StatelessWidget {
 final customColor = [0xFFB0BEC5,0xFF90A4AE,0xFF37474F,0xFF263238];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color (customColor[3]),
        title: Text("Profile"),
      ),
      body: Consumer<UserVO>(
        builder: (_, vo, __) => Center(
          child: Container(
            width: double.infinity,
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.topLeft,
                end: Alignment.bottomRight,
                stops: [0.1, 0.3, 0.5, 0.7,],
                colors: [
                  Color(customColor[0]),
                  Color(customColor[1]),
                  Color(customColor[2]),
                  Color(customColor[3]),
                ],
              ),
            ),
            child: Column(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: UserAvatar(108),
                ),

                Text("Welcome ${vo.name} ${vo.surname}",
                    style: TextStyle(
                        fontFamily: 'Grenze',
                        fontSize: 35,
                        fontWeight: FontWeight.bold,color: Colors.white)),

                Card(
                  margin: EdgeInsets.only(top: 100.0),
                  elevation: 8,
                  child: Container(
                    height: 50,
                    width: 300,
                    child: Center(
                      child: Text(
                        "email: ${vo.email}",
                        style: TextStyle(
                            fontSize: 20,
                            fontFamily: 'Grenze',
                            fontWeight: FontWeight.w400),
                      ),
                    ),
                  ),
                ),
                Card(
                  margin: EdgeInsets.only(top: 20.0),
                  elevation: 8,
                  child: Container(
                    height: 50,
                    width: 300,
                    child: Center(
                      child: Text(
                        "phone: ${vo.phone}",
                        style: TextStyle(
                            fontSize: 20,
                            fontFamily: 'Grenze',
                            fontWeight: FontWeight.w400),
                      ),
                    ),
                  ),
                ),
                //welcome $name $surname
              ],
            ),
          ),
        ),
      ),
    );
  }
}
